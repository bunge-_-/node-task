const express = require('express')
const app = express()
const port = 3000
/**
 * This is just some training im doing for school
 */
app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})